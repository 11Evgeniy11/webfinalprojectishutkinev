import io.qameta.allure.Feature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LoginPageTest extends AbstractTest {
    @BeforeEach
    void openLoginPage() {
        AbstractPage.getDriver().get(LoginPage.urlLoginPage);
    }

    @DisplayName("Валидная авторизация")
    @Test
    public void valid() {
        LoginPage.logIn(username, password);
        assertThat("Username should be " + username,
                AbstractPage.getDriver().findElement(MyPostsPage.userNameLink).getText(),
                is(equalTo("Hello, " + username)));
    }

    @DisplayName("Авторизация с invalid username")
    @Test
    public void invalidUsername() {
        LoginPage.logIn("aloxaman", password);
        assertThat("Should be a message about an invalid login and password",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Invalid credentials")));
    }

    @DisplayName("Авторизация с invalid password")
    @Test
    public void invalidPassword() {
        LoginPage.logIn(username, "qwerty12345");
        assertThat("Should be a message about an invalid login and password",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Проверьте логин и пароль")));
    }

    @DisplayName("Авторизация с пустыми значениями")
    @Test
    public void emptyValueAll() {
        LoginPage.logIn("", "");
        assertThat("Should be a message about empty fields",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Поле не может быть пустым")));
    }

    @DisplayName("Авторизация с пустым значением username")
    @Test
    public void emptyValueUsername() {
        LoginPage.logIn(username, "");
        assertThat("Should be a message about empty fields",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Поле не может быть пустым")));
    }

    @DisplayName("Авторизация с пустым значением password")
    @Test
    public void emptyValuePassword() {
        LoginPage.logIn("", password);
        assertThat("Should be a message about empty fields",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Поле не может быть пустым")));
    }

    @DisplayName("Авторизация с недопустимым username")
    @Test
    public void invalidValue() {
        LoginPage.logIn("привет", password);
        assertThat("Should be a message about invalid characters",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Неправильный логин. Может состоять только из латинских букв и цифр, без спецсимволов")));
         }



    @DisplayName("Авторизация с недопустимым граничным занчением: 2 символа")
    @Test
    public void twoCharacters() {
        LoginPage.logIn("da", password);
        assertThat("Should be a message about an invalid login length",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Неправильный логин. Может быть не менее 3 и не более 20 символов")));
    }

    @DisplayName("Авторизация с допустимым граничным значением: 3 символа")
    @Feature("Authorization")
    @Test
    public void threeCharacters() {
        LoginPage.logIn("pop", password);
        assertThat("Should be a message about an invalid login and password",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Проверьте логин и пароль")));
    }

    @DisplayName("Авторизация с допустимым граничным значением: 20 символов")
    @Test
    public void twentyCharacters() {
        LoginPage.logIn("abracadabrasimsalabi", password);
        assertThat("Should be a message about an invalid login and password",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Проверьте логин и пароль")));
    }

    @DisplayName("Авторизация с недопустимым граничным занчением: 21 символ")
    @Test
    public void twentyOneCharacters() {
        LoginPage.logIn("abracadabrasimsalabim", password);
        assertThat("Should be a message about an invalid login length",
                AbstractPage.getDriver().findElement(LoginPage.errorMessage).getText(),
                is(equalTo("Неправильный логин. Может быть не менее 3 и не более 20 символов")));
    }
}
