import io.qameta.allure.Feature;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;



public class MyPostsPageTest extends AbstractTest {
    @BeforeEach
    void openLoginPage() {

        AbstractPage.getDriver().get(LoginPage.urlLoginPage);
    }

    @DisplayName("Создание и удаления поста")
    @Test
    void createAndDeletePost() {
        LoginPage.logIn(username, password);
        AbstractPage.getDriver().findElement(MyPostsPage.btnCreateNewPost).click();
        AbstractPage.getDriver().findElement(MyPostsPage.inputTitle).sendKeys("Пост удалить");
        AbstractPage.getDriver().findElement(MyPostsPage.inputDescription).sendKeys("Удалить");
        AbstractPage.getDriver().findElement(MyPostsPage.btnSave).click();
        AbstractPage.getDriver().findElement(MyPostsPage.btnDeletePost).click();
        new Actions(AbstractPage.getDriver()).pause(1000).perform();
        assertThrows(WebDriverException.class,
                () -> AbstractPage.getDriver().findElement(By.xpath("//h1[.='Post for delete']")));
    }

    @DisplayName("Количество постов на одной странице")
    @Test
    void countPostCards() {
        LoginPage.logIn(username, password);
        List<WebElement> cards = AbstractPage.getDriver().findElements(MyPostsPage.cardPost);
        Integer countCards = cards.size();

        cards = AbstractPage.getDriver().findElements(MyPostsPage.cardPost);
        countCards = cards.size();
        assertEquals(10, countCards);
    }

    @DisplayName("Переход на следующую страницу и возврат на предыдущую")
    @Test
    void goToThePrevPage() {
        LoginPage.logIn(username, password);
        AbstractPage.getDriver().findElement(MyPostsPage.btnNextPage).click();
        AbstractPage.getDriver().findElement(MyPostsPage.btnPrevPage).click();
        AbstractPage.wait.until(ExpectedConditions.urlToBe(MyPostsPage.urlPrevPage));
        assertThat(AbstractPage.getDriver().getCurrentUrl(), is(equalTo(MyPostsPage.urlPrevPage)));
    }

    @DisplayName("Проверка сортировки (ASC)")
    @Test
    void sortingPostsByDateASC() {
        LoginPage.logIn(username, password);
        AbstractPage.getDriver().findElement(MyPostsPage.btnOrderDESC).click();
        AbstractPage.getDriver().findElement(MyPostsPage.btnOrderASC).click();
        assertThat(AbstractPage.getDriver().getCurrentUrl(), is(equalTo(MyPostsPage.urlASC)));
    }

    @DisplayName("Проверка сортировки (DESC)")
    @Test
    void sortingPostsByDateDESC() {
        LoginPage.logIn(username, password);
        AbstractPage.getDriver().findElement(MyPostsPage.btnOrderDESC).click();
        assertThat(AbstractPage.getDriver().getCurrentUrl(), is(equalTo(MyPostsPage.urlDESC)));
    }


    @DisplayName("Проверка чужих постов")
    @Test
    void openLoginNewPage() {
        LoginPage.logIn(username, password);
        AbstractPage.getDriver().findElement(MyPostsPage.btnNotMyPosts).click();
        assertThat(AbstractPage.getDriver().getCurrentUrl(), Matchers.is(Matchers.equalTo(MyPostsPage.urlNotMyPostsPage)));
    }
}